import React, {Component} from 'react';

class TodoForm extends Component{
	constructor(){
		super();
		this.state = {
			title:"",
			responsable:"",
			description:"",
			priority:"low"
		};
		this.handleInput = this.handleInput.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleInput(e){
		const {value,name} = e.target;
		this.setState({
			[name]:value
		});
	}

	handleSubmit(e){
		e.preventDefault();
		this.props.onAddTodo(this.state);
	}

	render(){
		return (
			<div className="card">
				<form className="card-body" onSubmit={this.handleSubmit}>
					<div className="form-group">
						<input onChange={this.handleInput} required="required" type="text" name="title" className="form-control" placeholder="Title" />
					</div>
					<div className="form-group">
						<input onChange={this.handleInput} required="required" type="text" name="responsable" className="form-control" placeholder="Responsable" />
					</div>
					<div className="form-group">
						<input onChange={this.handleInput} required="required" type="text" name="description" className="form-control" placeholder="Description" />
					</div>
					<div className="form-group">
						<select onChange={this.handleInput} required="required" name="priority" className="form-control">
							<option>low</option>
							<option>medium</option>
							<option>high</option>
						</select>
					</div>
					<button type="submit" className="btn btn-primary">Enviar</button>
				</form>
			</div>
		);
	}
}

export default TodoForm;